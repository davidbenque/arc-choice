# Data
This project uses Neo4j Community Edition 3.5.1, see [here](https://neo4j.com/docs/operations-manual/current/installation/) to install.

To load one of the backups in Neo4j:  
`bin/neo4j-admin load --from=_data/2019_03_27.dump --database=graph.db --force`  
and/or see the [manual](https://neo4j.com/docs/operations-manual/current/tools/dump-load/).

Each session is also logged as a text_file in [01-YT-map/graph_sessions](../01-YT-map/graph_sessions).

Data from the latest *traces* experiments (for RTD2019) haven't been imported in the Neo4j graph yet. They can be found as csv files in [03-YT-traces/web_traces/logs](../03-YT-traces/web_traces/logs).
