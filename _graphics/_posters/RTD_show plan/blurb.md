This system is an ongoing experiment in mapping YouTube recommendations, a series of computational diagrams that weave together the tools of computational prediction, critical design practice, and theory.

Our simple program continuously visits a video page, lists all the recommendations, chooses one at random, and repeats. In previous experiments, we specified a number of steps in order to produce static visualisations for our paper. Here we are experimenting with much longer run times to collect *traces* which allow for new types of *conjectures* on Youtube's system of algorithmic capture.

Code, data, and additional outputs/material are available at:  
https://gitlab.com/davidbenque/arc-choice