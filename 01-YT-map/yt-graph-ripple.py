
from yt_graph_tools import *

from selenium import webdriver
from random import shuffle

link_props['sesh_type'] = 'ripple'
# nr_rounds = 1

print('Starting session: ', sesh_stamp)
print('Mode: ', link_props['sesh_type'])

# start log file
log_filename = datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + '.txt'
log = open(log_filename, 'w')
log_header(log, link_props)

# %%
# set up drivers
options = webdriver.ChromeOptions()
options.add_argument('headless')

yt_home = webdriver.Chrome(chrome_options=options)
vid_page = webdriver.Chrome(chrome_options=options)
rec_page = webdriver.Chrome(chrome_options=options)

# load Youtube homepage
yt_home.get("https://youtube.com/")
yt_home.implicitly_wait(3)

# list videos on homepage
thumbs = yt_home.find_elements_by_id("details")

edges = []

# %%
# follow links to video pages
for t in thumbs:
    a_tag = t.find_element_by_tag_name('a')
    vid_url = a_tag.get_attribute("href")

    node_props = get_vid_data(vid_page, vid_url)
    make_node(node_props)

    source_id = node_props['YT_id'] # save start node id for links

    link_to_home(source_id, link_props)

    log.write('Start: ' + node_props['title'] + ' - ' + node_props['YT_id'] + '\n')

    # get sidebar recommendations 
    watch_next = vid_page.find_element_by_tag_name("ytd-watch-next-secondary-results-renderer")
    recs = watch_next.find_elements_by_tag_name("ytd-compact-video-renderer")

    for rec in recs:
        rec_url = rec.find_element_by_tag_name("a").get_attribute("href")
        rec_props = get_vid_data(rec_page, rec_url)
        make_node(rec_props)
        make_link(source_id, rec_props['YT_id'], link_props)

# %%


# for n in range(nr_rounds):
#     print('round {n} of {total}'.format(n=n+1, total=nr_rounds))

#     # get all sidebar recommendations 
#     watch_next = vid_page.find_element_by_tag_name("ytd-watch-next-secondary-results-renderer")
#     recs = watch_next.find_elements_by_tag_name("ytd-compact-video-renderer")
#     shuffle(recs)
#     log.write('{i} recommendations\n'.format(i=len(recs)))
#     # create nodes and links
#     for rec in recs:
#         rec_url = rec.find_element_by_tag_name("a").get_attribute("href")
#         rec_props = get_vid_data(rec_page, rec_url)
#         make_node(rec_props)
#         make_link(source_id, rec_props['YT_id'], link_props)

#     # click last video to continue
#     make_link(source_id, rec_props['YT_id'], link_props, label='CLICKED')

#     # this moves the vid_page driver to the new video
#     next_props = get_vid_data(vid_page, rec_url)
#     source_id = rec_props['YT_id']

#     log.write('round {n} of {total} done \n'.format(n=n+1, total=nr_rounds))
#     log.write('next: ' + next_props['title'] + ' - ' + next_props['YT_id'] + '\n')


# %% Tear it down 
log.close()
yt_home.quit()
vid_page.quit()
rec_page.quit()
print('Done')
