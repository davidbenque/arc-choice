import html
from py2neo import Graph, Relationship, NodeSelector, authenticate
from datetime import datetime
import re
from json import load
from urllib.request import urlopen


# %% Login to Neo4j

f = open('neo4j-creds.txt', 'r')
creds = f.readlines()
username = creds[0].rstrip()
password = creds[1].rstrip()

authenticate("localhost:7474", username, password)
graph = Graph("http://localhost:7474/db/data/")
selector = NodeSelector(graph)

# Session timestamp
sesh_stamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# Session IP
# https://ipapi.co/api/#location-of-clients-ip
ip_query = load(urlopen('http://ipapi.co/json/'))
ip = ip_query["ip"]
isp = ip_query["org"]
ip_region = ip_query["region"]

link_props = {
    'sesh_id': sesh_stamp,
    'sesh_ip': ip,
    'sesh_isp': isp,
    'sesh_loc': ip_region
}

#===============================
# Utility Functions
#===============================

def log_header(file, props_dict):
    ''' Write all session properties at the top of the log file '''
    for key, value in props_dict.items():
        file.write(key + ": " + value + "\n")
    file.write("--------------------\n\n")

#===============================
# Graph Functions
#===============================

def link_to_home(target_id, props):
    ''' links target_id to the homepage node
    all sessions should start with this'''

    home = selector.select('YT_homepage', YT_id='home')
    end_video = selector.select('Video', YT_id=target_id)

    home = list(home)[0]
    end_video = list(end_video)[0]
    rel = Relationship(home, "CLICKED", end_video, **link_props)
    graph.create(rel)


def make_node(props):
    ''' find or create video node
        matching with YT id which should be unique
        update the properties '''

    merge_q = '''
        MERGE (a:Video { YT_id: {id}})
        SET a = {node_props}
        '''
    q = graph.run(merge_q, id=props['YT_id'], node_props=props)


def make_link(source_id, target_id, props, label='TO'):
    ''' Link two videos by their YT ids
        will create link even if it already exists
    '''
    start_video = selector.select('Video', YT_id=source_id)
    end_video = selector.select('Video', YT_id=target_id)

    start_video = list(start_video)[0]
    end_video = list(end_video)[0]

    rel = Relationship(start_video, label, end_video, **link_props)
    graph.create(rel)


def get_vid_data(driver, vid_url):
    ''' Takes in a selenium driver and video url
    Returns title, views, id and timestamp as dict
    '''

    driver.get(vid_url)
    driver.implicitly_wait(5)

    id = vid_url.split('v=')[1]

    title_tag = driver.find_element_by_tag_name("h1")
    title_tag = title_tag.find_element_by_tag_name("yt-formatted-string")
    title = title_tag.get_attribute('innerHTML')

    view_tag = driver.find_element_by_tag_name("yt-view-count-renderer")
    view_counter = view_tag.find_element_by_class_name("view-count")
    views_html = view_counter.get_attribute('innerHTML')
    v = re.findall(r'\d+', views_html)
    views_s = ''.join(v)
    views = int(views_s)

    # uploader name
    meta = driver.find_element_by_tag_name("ytd-video-owner-renderer")
    owner_info = meta.find_element_by_id("owner-name")
    owner_name = owner_info.find_element_by_tag_name('a').get_attribute('innerHTML')
    # date
    date_string = meta.find_element_by_class_name("date").get_attribute('innerHTML')
    r = re.compile('\d+\s\w+\s\d+')
    date = r.search(date_string).group(0)

    node_props = {
        'title': html.unescape(title),
        'views': views,
        'by': owner_name,
        'date': date,
        'last_visited': sesh_stamp,
        'YT_id': id}

    return node_props
