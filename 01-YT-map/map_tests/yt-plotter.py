import time

import pandas as pd
import numpy as np
import networkx as nx


# %%
df = pd.read_csv("2L-1-2018-07-13-14-29-51.csv", usecols=[1,2,3], dtype=object)


# %%

G = nx.from_pandas_dataframe(df, "source", "target")

# %%
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")

fig = plt.figure(figsize=(50,50))


# graphviz
pos = nx.drawing.pygraphviz_layout(G, prog="sfdp")
nx.draw(G,pos,node_color='k',node_size=15, with_labels=False)

# ego graph
# hub_ego = nx.ego_graph(G, "start", radius=10)
# pos = nx.spring_layout(hub_ego)
# nx.draw(hub_ego,pos,node_color='k',node_size=10, with_labels=False)

# regular G
# pos = nx.spring_layout(G)
# nx.draw(G,pos,node_color='k',node_size=10, with_labels=False)

# Draw start node as large and red
nx.draw_networkx_nodes(G,pos,nodelist=["start"],node_size=600,node_color='r')
# nx.draw_networkx_labels(G,pos)

from datetime import datetime

filename = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
plt.savefig(filename + "-SFDPtest.png")
#df.to_csv("2-levels-" + filename + ".csv")

#plt.show()
