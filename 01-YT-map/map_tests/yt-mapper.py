import time

import pandas as pd
import networkx as nx

from selenium import webdriver

# %%
options = webdriver.ChromeOptions()
options.add_argument('headless')

yt_home = webdriver.Chrome(chrome_options=options)
vid_page = webdriver.Chrome(chrome_options=options)
sub_vid_page = webdriver.Chrome(chrome_options=options)

# print("Cookies: ", yt_home.get_cookies(), vid_page.get_cookies())

yt_home.get("https://youtube.com/")
time.sleep(3)
# %%
thumbs = yt_home.find_elements_by_id("details")

# %%
edges = []

for t in thumbs:
    a_tag = t.find_element_by_tag_name('a')
    vid_title = a_tag.get_attribute("title")
    vid_url = a_tag.get_attribute("href")
    edges.append({
        "source": "start",
        "target": vid_title,
        "target_url": vid_url
        })

    print(vid_title, vid_url)
    vid_page.get(vid_url)
    time.sleep(2)
    #print("Recommendations: ")

    recs = vid_page.find_elements_by_tag_name("ytd-compact-video-renderer")
    for rec in recs:
        rec_title = rec.find_element_by_id("video-title").get_attribute("title")
        rec_url = rec.find_element_by_tag_name("a").get_attribute("href")
        print("-" + rec_title, rec_url)
        edges.append({
            "source": vid_title,
            "target": rec_title,
            "target_url": rec_url
            })

        sub_vid_page.get(rec_url)
        time.sleep(2)
        sub_recs = sub_vid_page.find_elements_by_tag_name("ytd-compact-video-renderer")
        # for sub_rec in sub_recs:
        sub_rec = sub_recs[0]
        sub_rec_title = sub_rec.find_element_by_id("video-title").get_attribute("title")
        sub_rec_url = sub_rec.find_element_by_tag_name("a").get_attribute("href")
        print("--" + sub_rec_title, sub_rec_url)
        edges.append({
            "source": rec_title,
            "target": sub_rec_title,
            "target_url": sub_rec_url
            })



    #print()

yt_home.quit()
vid_page.quit()

# %%

df = pd.DataFrame(edges)

# %%

G = nx.from_pandas_dataframe(df, "source", "target")

# %%
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")

fig = plt.figure(figsize=(50,50))

# hub_ego = nx.ego_graph(G, "start", radius=10)
# Draw graph
pos = pos=nx.drawing.pygraphviz_layout(G, prog="twopi")
nx.draw(G,pos,node_color='k',node_size=15, with_labels=False)
# Draw ego as large and red
nx.draw_networkx_nodes(G,pos,nodelist=["start"],node_size=600,node_color='r')
# nx.draw_networkx_labels(G,pos)

from datetime import datetime

filename = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
plt.savefig("2L-1-" + filename + ".png")
df.to_csv("2L-1-" + filename + ".csv")


#plt.show()
