import pandas as pd
import networkx as nx

from selenium import webdriver
from datetime import datetime

import time

# %%
options = webdriver.ChromeOptions()
options.add_argument('headless')

yt_home = webdriver.Chrome(chrome_options=options)
vid_page = webdriver.Chrome(chrome_options=options)

# print("Cookies: ", yt_home.get_cookies(), vid_page.get_cookies())

yt_home.get("https://youtube.com/")
time.sleep(5)

# %%
thumbs = yt_home.find_elements_by_id("details")

# %%
def mermaid_tag(id, label):
    return id + '["' + label.replace('"',"'") + '"]'

# %%
filename = "mermaid_out" #datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
f = open(filename + ".mmd", 'wb')
f.write("graph LR\n".encode("utf8"))

start = 'start["Youtube homepage"]'

for t in thumbs:
    a_tag = t.find_element_by_tag_name('a')
    vid_title = a_tag.get_attribute("title")
    vid_url = a_tag.get_attribute("href")
    vid_id = vid_url.split("=")[1]
    line = start + "-->" + mermaid_tag(vid_id, vid_title) + "\n"
    f.write(line.encode('utf8'))
    #print(vid_title, vid_url)
    vid_page.get(vid_url)
    time.sleep(5)
    #print("Recommendations: ")

    recs = vid_page.find_elements_by_tag_name("ytd-compact-video-renderer")
    for rec in recs:
        rec_title = rec.find_element_by_id("video-title").get_attribute("title")
        rec_url = rec.find_element_by_tag_name("a").get_attribute("href")
        rec_id = rec_url.split("=")[1]
        line = mermaid_tag(vid_id, vid_title) + "-->" + mermaid_tag(rec_id, rec_title)+ "\n"
        f.write(line.encode('utf8'))
        #edges.append({"source": vid_title, "target": rec_title, "target_url": rec_url})
        #print(rec_title, rec_url)
    #print()

yt_home.quit()
vid_page.quit()

f.close()
