First set of proof-of-concept experiments with matplotlib and mermaid.js.  
to see the output of these scripts see [02-YT-viz/matplotlib_outputs](../../02-YT-viz/matplotlib_outputs) and [02-YT-viz/mermaid_outputs](../../02-YT-viz/mermaid_outputs).
