import time
from datetime import datetime

from selenium import webdriver

from random import shuffle

# %%
options = webdriver.ChromeOptions()
options.add_argument('headless')

yt_home = webdriver.Chrome(chrome_options=options)
vid_page = webdriver.Chrome(chrome_options=options)
sub_vid_page = webdriver.Chrome(chrome_options=options)

filename = "Digger-at-" + datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + ".txt"
file = open(filename, "w")

# print("Cookies: ", yt_home.get_cookies(), vid_page.get_cookies())

yt_home.get("https://youtube.com/")
time.sleep(3)
# %%
thumbs = yt_home.find_elements_by_id("details")

# %%

shuffle(thumbs)

t = thumbs[0]
a_tag = t.find_element_by_tag_name('a')
vid_title = a_tag.get_attribute("title")
vid_url = a_tag.get_attribute("href")

print(vid_title, vid_url)
file.write(vid_title + " - " + vid_url + "\n")

vid_page.get(vid_url)
time.sleep(2)
    #print("Recommendations: ")
for n in range(200):
    recs = vid_page.find_elements_by_tag_name("ytd-compact-video-renderer")
    shuffle(recs)

    rec = recs[0]
    rec_title = rec.find_element_by_id("video-title").get_attribute("title")
    rec_url = rec.find_element_by_tag_name("a").get_attribute("href")

    print(rec_title, rec_url)
    file.write(rec_title + " - " + rec_url + "\n")
    vid_page.get(rec_url)
    time.sleep(2)

yt_home.quit()
vid_page.quit()
file.close()
