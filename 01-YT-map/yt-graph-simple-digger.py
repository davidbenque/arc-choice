
from yt_graph_tools import *

from selenium import webdriver
from random import shuffle

link_props['sesh_type'] = 'simple_digger'
nr_rounds = 15

print('Starting session: ', sesh_stamp)
print('Mode: ', link_props['sesh_type'])

# start log file
sesh_stamp = sesh_stamp.replace(":", "-")
sesh_stamp = sesh_stamp.replace(" ", "-")
log_filename = sesh_stamp + '.txt'
log = open(log_filename, 'w')
log_header(log, link_props)

# %%
# set up drivers
options = webdriver.ChromeOptions()
options.add_argument('headless')

yt_home = webdriver.Chrome(chrome_options=options)
vid_page = webdriver.Chrome(chrome_options=options)

# load Youtube homepage
yt_home.get("https://youtube.com/")
yt_home.implicitly_wait(5)

# pick a video at random
thumbs = yt_home.find_elements_by_id("details")
shuffle(thumbs)
t = thumbs[0]

# %%
# follow link to video page
a_tag = t.find_element_by_tag_name('a')
vid_url = a_tag.get_attribute("href")

node_props = get_vid_data(vid_page, vid_url)
make_node(node_props)

source_id = node_props['YT_id'] # save start node id for links

link_to_home(source_id, link_props)

log.write('Start: ' + node_props['title'] + ' - ' + node_props['YT_id'] + '\n')

# %%

for n in range(nr_rounds):
    print('round {n} of {total}'.format(n=n+1, total=nr_rounds))

    # get all sidebar recommendations 
    watch_next = vid_page.find_element_by_tag_name("ytd-watch-next-secondary-results-renderer")
    recs = watch_next.find_elements_by_tag_name("ytd-compact-video-renderer")

    # pick a video at random
    shuffle(recs)
    rec = recs[0]
    
    rec_url = rec.find_element_by_tag_name("a").get_attribute("href")
    rec_props = get_vid_data(vid_page, rec_url)
        
    make_node(rec_props)
    print(rec_props['title'])
    make_link(source_id, rec_props['YT_id'], link_props, label='CLICKED')
    source_id = rec_props['YT_id']

    log.write('round {n} of {total} done \n'.format(n=n+1, total=nr_rounds))
    log.write('processed: ' + rec_props['title'] + ' - ' + rec_props['YT_id'] + '\n')


# %% Tear it down 
log.close()
yt_home.quit()
vid_page.quit()
print('Done')
