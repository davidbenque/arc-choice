
# Architectures of Choice - Vol.1: Youtube  

Any interface to a vast catalog of media is, explicitly or not, an ordering of choices. This project presents a series of experiments in mapping recommendations on the YouTube platform. It acts as a starting point towards a visual understanding of how choice is directed, generated, and controlled by algorithmic systems.

This project was initially commissioned as a partnership between [Supra Systems Studio](http://suprasystems.studio/) and [BBC R&D](https://www.bbc.co.uk/rd) for *Everything Happens So Much*, an exhibition at the London College of Communication (London Design Festival, Sept. 2018). The last section on *traces* is the result of further development and reflection in collaboration with Betti Marenko for the Research Through Design conference (March 2019).

for further information see [my website](https://davidbenque.com/projects/architectures-of-choice/) and our Research Through Design paper:

Marenko, B. and Benqué, D. (2019) ‘Speculative diagrams: Experiments in mapping Youtube’, in *Method & Critique; frictions and shifts in RTD*. *Research Through Design*, TU Delft. doi: <https://doi.org/10.6084/m9.figshare.7855811.v1>.


## Structure of the repository

### Scraping [01-YT-map](01-YT-map)  

Three *probes* gather Youtube recommendation data, using headless web-browser Selenium. The data are written to a Neo4j graph database (see [_data](_data)).

`ripple.py` attempts to map all recommendations for n levels  
`digger.py` logs all recommendations but only follows one random link  
`simple_digger.py` only follows one random link

![](_graphics/_probe_diagrams/artwork/all.png)

### Visualisation [02-YT-viz](02-YT-viz)   

Attempts to visualise data from the graph using tools such as:

- Neo4j browser
- Python: matplotlib, networkx, datashader
- Mermaid.js
- Gephi

![](_graphics/_posters/_instagram/preview_comp.png)

### Traces [03-YT-traces](03-YT-traces)

Focused visualisations of single paths through Youtube, showing details of each video such as title, author, and views. These are shown as static images of short traces, and animations of much longer ones.

![](03-YT-traces/outputs/2019-01-21-12-11-06.png)

![](03-YT-traces/outputs/20190328_random_screens_[6x3HD]/screenshot-1.png)

![](03-YT-traces/outputs/20190227_sequence_[3x3]/new_seq_slow.mp4)


## Installation
### Database
Most of this project works with a Neo4j graph database (Community Edition 3.5.1). To install see [their manual](https://neo4j.com/docs/operations-manual/current/installation/).

If you do not want to install Neo4j, try the [web visualisation](03-YT-traces/web_traces) in the *Traces* section which only stores data in .csv files.

### Python
The Conda environment provided: `BBCTV_env.yml` was used for early experiments with various software and is therefore bloated with unused packages. I then moved to Pipenv after failing to replicate the Conda env on a new machine. Pipfile is much leaner but may be missing some packages depending on which scripts you are trying to run.

To install using Pipenv run the following from the repository root:   
`pipenv install` adding `--dev` to also install an iPython kernel

Most of the Python code is the result of explorations using the [Hydrogen](https://atom.io/packages/hydrogen) package for Atom. This accounts for some of the spaghetti coding style and for the `# %%` markers that separate notebook cells. The code should still run in other IDEs or from the command line.

## Data
Please see the [data](_data) directory for instructions on how to load the graph database in Neo4j.
