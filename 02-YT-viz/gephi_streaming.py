'''
stream the data from a Neo4j cypher query to Gephi for visualisation
'''

from py2neo import Graph, Relationship, NodeSelector, authenticate
import pandas as pd
import os

# %%

f = open('neo4j-creds.txt', 'r')

creds = f.readlines()
username = creds[0].rstrip()
password = creds[1].rstrip()

authenticate("localhost:7474", username, password)
neo4j_graph = Graph("http://localhost:7474/db/data/")


#%%
from gephistreamer import graph
from gephistreamer import streamer

# Create a Streamer
# adapt if needed : streamer.GephiWS(hostname="localhost", port=8080, workspace="workspace0")
# You can also use REST call with GephiREST (a little bit slower than Websocket)
stream = streamer.Streamer(streamer.GephiREST())
# %%


# multiple sessions query - for simple_diggers
query = '''
        match (a)-[r]-(b)
        where r.sesh_id IN ['2018-09-11 14:12:50','2018-09-11 14:15:01', '2018-09-11 14:17:00', '2018-09-11 14:18:47', '2018-09-11 14:20:40', '2018-09-11 14:23:09', '2018-09-11 14:38:55', '2018-09-11 14:41:49', '2018-09-11 14:46:04', '2018-09-11 14:49:35', '2018-09-11 14:52:32']
        return a, r, b, type(r)
        '''


# One session query - for ripple sessions
# query = '''
#         match (a)-[r]-(b)
#         where r.sesh_id = '2018-09-11 11:18:06'
#         return a, r, b, type(r)
#         '''

# Full Graph Query
# query='''
#     MATCH (a)-[r]->(b)
#     WHERE a:Video OR a:YT_homepage
#     RETURN a, r, b, type(r)
#     '''

results = pd.DataFrame(neo4j_graph.run(query).data())

# %%
results.head()

# %%
test = results.iloc[0]
print(test.a['YT_id'])

len(results)

# %%

for index, row in results.iterrows():
    a, b, r = row['a'], row['b'], row['r']
    r['type'] = row['type(r)']
    node_a = graph.Node(a['YT_id'], **a)
    node_b = graph.Node(b['YT_id'], **b)
    edge_ab = graph.Edge(node_a, node_b, **r)
    if r['type'] == "CLICKED":
        edge_ab.property['weight']=2
    else:
        edge_ab.property['weight']=1
    stream.add_node(node_a, node_b)
    stream.add_edge(edge_ab)

# %%
#
# query='''
#     MATCH (a:YT_homepage)-[r]->(b)
#     RETURN a, r, b, type(r)
#     '''
#
# results = pd.DataFrame(neo4j_graph.run(query).data())
#


# # %%
# # example code
#
# # Create a node with a custom_property
# node_a = graph.Node("A",custom_property=1)
#
# # Create a node and then add the custom_property
# node_b = graph.Node("B")
# node_b.property['custom_property']=2
#
# # Add the node to the stream
# # you can also do it one by one or via a list
# # l = [node_a,node_b]
# # stream.add_node(*l)
# stream.add_node(node_a,node_b)
#
# # Create edge
# # You can also use the id of the node : graph.Edge("A","B",custom_property="hello")
# edge_ab = graph.Edge(node_a,node_b,custom_property="hello")
# stream.add_edge(edge_ab)
