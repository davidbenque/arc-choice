'''
failed attempt at clustering the video titles.
'''

from py2neo import Graph, Relationship, NodeSelector, authenticate
import pandas as pd
import os

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score

# %%

f = open('neo4j-creds.txt', 'r')

creds = f.readlines()
username = creds[0].rstrip()
password = creds[1].rstrip()

authenticate("localhost:7474", username, password)
graph = Graph("http://localhost:7474/db/data/")


#%%

query='''
    MATCH (a:Video)-[r]-(b)
    RETURN ID(a) as id, a.title as title, r.sesh_id as sesh
    '''

results = pd.DataFrame(graph.run(query).data())

#%%
results.describe()

#%%

documents = results.title.values

vectorizer = TfidfVectorizer(
    max_df=0.3,
    max_features=200000,
    min_df=5,
    stop_words='english',
    ngram_range=(1,4))

X = vectorizer.fit_transform(documents)

true_k = 20# len(results.sesh.unique())
model = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1)
model.fit(X)

print("Top terms per cluster:")
order_centroids = model.cluster_centers_.argsort()[:, ::-1]
terms = vectorizer.get_feature_names()
for i in range(true_k):
    print("Cluster %d:" % i),
    for ind in order_centroids[i, :10]:
        print(' %s' % terms[ind]),
    print

#
# print("\n")
# print("Prediction")
#
# Y = vectorizer.transform(["chrome browser to open."])
# prediction = model.predict(Y)
# print(prediction)
#
# Y = vectorizer.transform(["My cat is hungry."])
# prediction = model.predict(Y)
# print(prediction)
