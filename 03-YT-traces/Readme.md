Here are the latest developments of the project, as it was revisited for the Research Through Design conference 2019. The focus here is on individual *traces* rather than an exhaustive mapping of Youtube. 

The print version queries the Neo4j graph to produce PDF visualisations of individual sessions.

The web version is a standalone project made for the Research Through Design exhibition. It queries Youtube directly and produces a live visualisation in the browser. It logs paths through Youtube as .csv files which can also be replayed in the same interface.
