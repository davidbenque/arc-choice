from py2neo import Graph, Relationship, NodeSelector, authenticate
import os
import json

# %% Neo4j login
username = os.environ["NEO4J_USER"]
password = os.environ["NEO4J_PASS"]

authenticate("localhost:7474", username, password)
neo4j_graph = Graph("http://localhost:7474/db/data/")

# %%
'''
Export json for D3 from a simple_digger session
(only returns one clicked path)
'''


sesh_id = "2019-01-21 12:14:58"
query = f'''
    MATCH (source)-[r:CLICKED]->(target)
    WHERE r.sesh_id = "{sesh_id}"
    RETURN ID(source), properties(source), ID(r), properties(r), type(r), ID(target), properties(target)
    ORDER BY ID(r)
    '''

results = neo4j_graph.run(query).data()

# %%
nodes_temp = []
nodes = []
links = []

# reindexing needed for D3 v3
def node_index(node_id):
    return next((index for (index, d) in enumerate(nodes) if d["id"] == node_id), None)

for result in results:
    source_node = {
        "id": result["ID(source)"],
        "properties": result["properties(source)"],
        "text": result["properties(source)"]["title"]
    }
    target_node = {
        "id": result["ID(target)"],
        "properties": result["properties(target)"],
        "text": result["properties(target)"]["title"]
    }
    nodes_temp.append(source_node)
    nodes_temp.append(target_node)

    # remove duplicate nodes
    for node in nodes_temp:
        if node not in nodes:
            nodes.append(node)

    link = {
        "id": result["ID(r)"],
        "source": node_index(result["ID(source)"]), # reindex for d3v3
        "target": node_index(result["ID(target)"]),
        "properties": result["properties(r)"],
        "type": result["type(r)"]
    }

    links.append(link)



data = {"nodes": nodes, "links": links}

with open('trace.json', 'w') as outfile:
    json.dump(data, outfile)
