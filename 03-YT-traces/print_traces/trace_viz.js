
var node_size = 180;

var cols = 4;
var gap = node_size + 50;
var padding = 30;


d3.json("trace.json", function(json) {

  var num_nodes = Object.keys(json.nodes).length;
  var width = (function(){
    return cols * gap + padding;
  })();

  var height = (function(){
    return Math.ceil(num_nodes / cols) * gap + padding;
  })();

  var svg = d3.select("#container").append("svg")
      .attr("width", width)
      .attr("height", height);

  svg.append("svg:defs").selectAll("marker")
      .data(["end"])      // Different link/path types can be defined here
    .enter().append("svg:marker")    // This section adds in the arrows
      .attr("id", String)
      .attr("viewBox", "0 -20 40 40")
      .attr("refX", 10)
      .attr("refY", 0)
      .attr("markerWidth", 40)
      .attr("markerHeight", 40)
      .attr("orient", "auto")
    .append("svg:path")
      .attr("d", "M0,-5 L10,0L0,5");

var title_info = json.links[0].properties;

var title = d3.select("#title").append("p")
    .html('Trace: ' + title_info.sesh_id + '<br>' +
          'Type: ' + title_info.sesh_type + '<br>' +
          'ISP: ' + title_info.sesh_isp + ' - Location: ' + title_info.sesh_loc)

// assign positions

json.nodes.forEach(function(e){
  e.x = e.y = padding;
  e.x += json.nodes.indexOf(e) % cols * gap;
  e.y += Math.floor(json.nodes.indexOf(e) / cols) * gap;
  });

json.links.forEach(function(link){
  var sourceNode = json.nodes.filter(function(d, i) {
    return i == link.source
  })[0];
  var targetNode = json.nodes.filter(function(d, i) {
    return i == link.target
  })[0];

  link.x1 = (function() {
    if (sourceNode.x < targetNode.x){
      return sourceNode.x + node_size;
      } else { return sourceNode.x }
  })();
  link.y1 = (function() {
    if(sourceNode.y + node_size < targetNode.y){
      return sourceNode.y + node_size;
    } if(sourceNode.y  > targetNode.y + node_size){
      return sourceNode.y;
    } else { return sourceNode.y + node_size/2;}
  })();
  link.x2 = (function() {
    if (targetNode.x + node_size < sourceNode.x){
      return targetNode.x + node_size;
    } else { return targetNode.x }
  })();
  link.y2 = (function() {
    if(targetNode.y > sourceNode.y  + node_size ){
      return targetNode.y ;
    } if(targetNode.y + node_size < sourceNode.y){
      return targetNode.y + node_size ;
    } else { return targetNode.y + node_size/2;}
  })();

});


  var links = svg.selectAll(".link")
      .data(json.links)
    .enter().append("line")
      .attr("class", "link")
      .attr("x1", function(d){return d.x1})
      .attr("y1", function(d){return d.y1})
      .attr("x2", function(d){return d.x2})
      .attr("y2", function(d){return d.y2})
      .attr("marker-end", "url(#end)");

  var node = svg.selectAll(".node")
      .data(json.nodes)
    .enter().append("g")
      .attr("class", "node");


 node.append("rect")
      .attr("width",node_size)
      .attr("height", node_size)
      .attr("class", function(d){
        if (d.text == "Youtube homepage"){ return "yt-home"} else {return "video"}
      })
      .attr("x", function(d) {
        return d.x
      })
      .attr("y", function(d) {
        return d.y
      });

  node.append("text")
      .text(function(d) { return d.text })
      .attr("class", function(d){
        if (d.text == "Youtube homepage"){ return "YT-home"} else { return "video-title" }
      })
      .each(function() {
          d3plus.textwrap().container(d3.select(this))
          .shape("square")
          .align("left")
          .padding(5)
          .valign("top")
          .draw();
        });


var meta_node = svg.selectAll(".meta-node")
    .data(json.nodes)
  .enter().append("g")
    .attr("class", "meta-node");

meta_node.append("rect")
     .attr("width",node_size)
     .attr("height", node_size)
     .attr("x", function(d) {
       return d.x
     })
     .attr("y", function(d) {
       return d.y
     });

 meta_node.append("text")
     .text(function(d) {
       var byline = d.properties.by;
       if(typeof byline == "undefined"){return '📺' } else { return byline }
       })
     .each(function() {
         d3plus.textwrap().container(d3.select(this))
         .shape("square")
         .align("left")
         .padding(5)
         .valign('bottom')
         .draw();
       });

var meta =   meta_node.append("text")
    .attr("x", function(d) {
      return d.x + 5;
    })
    .attr("y", function(d) {
      return d.y + (node_size)
    });

  meta.append("tspan")
  .attr("x", function(d){return d.x + 5})
    .attr("dy", "-4em")
    .text(function(d){
      return d.properties.date;
    })

  meta.append("tspan")
  .attr("x", function(d){return d.x + 5})
    .attr("dy", "1.3em")
    .text(function(d){
      var views = d.properties.views;
      if (typeof views !== 'undefined'){
      return "👁 " + numberWithCommas(d.properties.views)}
    })

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

});
