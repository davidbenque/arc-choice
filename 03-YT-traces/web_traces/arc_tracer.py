from selenium import webdriver
from random import shuffle, randint
import json
import csv
from helpers import *
import traceback
from time import gmtime, strftime

# %% meta data

start_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
step_count = 1

meta = {'start_time': start_time, 'steps': step_count}

# %%  set up drivers
options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument("--incognito")

yt_home = webdriver.Chrome(chrome_options=options)
vid_page = webdriver.Chrome(chrome_options=options)
vid_page.implicitly_wait(3)

# %% Settings
settings = {
    'buffer_size': 7,
    'node_size': 240,
    'cols': 6,
    'rows': 3,
    'gap': 50,
    'padding': 30
    }

# Full Screen 1920*1080 settings
# settings = {
#     'buffer_size': 5,
#     'node_size': 240,
#     'cols': 6,
#     'rows': 3,
#     'gap': 50,
#     'padding': 30
#     }

# %%
grid = set(node_grid(settings))

# %%

nodes = []
links = []

try:
    # %% Inital setup from YT homepage

    # load Youtube homepage
    yt_home.get("https://youtube.com/")
    yt_home.implicitly_wait(5)

    # pick a video at random
    thumbs = yt_home.find_elements_by_id("details")
    shuffle(thumbs)
    t = thumbs[0]

    # follow link to video page
    a_tag = t.find_element_by_tag_name('a')
    vid_url = a_tag.get_attribute("href")

    # first node
    node_props = get_vid_data(vid_page, vid_url)

    # %% start position on matrix
    mx = 0
    my = 0
    current_pos = (mx, my)
    pos = pix_pos(current_pos, settings) # pixel position

    node_ID = 0

    node = {
        'id': node_ID,
        'text': node_props.pop('title'),
        'properties': node_props,
        'mx': mx,
        'my': my,
        'x': pos[0],
        'y': pos[1],
        'age': 0
    }

    nodes.append(node)
    node_ID += 1

    ## start a logfile
    log_file_name = strftime("%Y-%m-%d-%H-%M-%S", gmtime())

    with open('./logs/' + log_file_name + '.csv', 'w') as f:
        w = csv.DictWriter(f, node.keys())
        w.writeheader()
    f.close()

    # %% Loop
    while True:
        meta['steps'] += 1

        for node in nodes:
            node['age'] += 1

        # get all sidebar recommendations
        recs = load_recs(vid_page)
        # pick a video at random
        shuffle(recs)
        rec = recs[0]

        # follow link to video and get data
        rec_url = rec.find_element_by_tag_name("a").get_attribute("href")
        rec_props = get_vid_data(vid_page, rec_url)

        # positioning
        next_cells = set(neighbors(current_pos,settings))

        taken = set(taken_cells(nodes))
        free_neighbors = list(next_cells - taken)

        if len(free_neighbors) > 0:
            shuffle(free_neighbors)
            current_pos = free_neighbors[0]
        else:
            free_cells = list(grid - taken)
            shuffle(free_cells)
            current_pos = free_cells[0]

        mx, my = current_pos
        posx, posy = pix_pos(current_pos, settings)

        rec_node = {
            'id': node_ID,
            'text': rec_props.pop('title'),
            'properties': rec_props,
            'mx': mx,
            'my': my,
            'x': posx,
            'y': posy,
            'age': 0
        }
        nodes.append(rec_node)
        print(node_ID, rec_props['by'][:4], rec_node['text'])

        with open('./logs/' + log_file_name + '.csv', 'a') as f:
            w = csv.DictWriter(f, rec_node.keys())
            w.writerow(node)
        f.close()

        if len(nodes) > settings['buffer_size']:
            nodes.pop(0)

        # generate link array from node list - assumes linear progression
        links = [] # reset
        for v, w in zip(nodes[:-1], nodes[1:]):
            link = {'source': nodes.index(v), 'target': nodes.index(w)}
            links.append(link)

        node_ID += 1
        data = {"meta": meta,
                "settings":settings,
                "nodes": nodes,
                "links": links
                }

        with open('live_trace.json', 'w') as outfile:
            json.dump(data, outfile)

except KeyboardInterrupt:
    # traceback.print_exc()
    yt_home.quit()
    vid_page.quit()
    print('-- Drivers terminated on keyboard interrupt --')

except Exception:
    traceback.print_exc()
    yt_home.quit()
    vid_page.quit()
    print('-- Drivers terminated on error --')

yt_home.quit()
vid_page.quit()
print('-- Drivers terminated on end --')
