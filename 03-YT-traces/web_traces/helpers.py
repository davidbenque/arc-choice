import re
import html
import itertools

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def pix_pos(matrix_pos, settings):
    '''
    input: matrix position [row,col], settings dict
    output: pixel position [x,y]
    '''
    f = settings['node_size'] + settings['gap']
    pix_pos = [n * f for n in matrix_pos]
    return pix_pos


def node_index(node_id, nodes):
    '''
    reindexing of nodes for D3 V3
    returns the index of node with node_id in nodes array
    '''
    return next((index for (index, d) in enumerate(nodes) if d["id"] == node_id), None)


def node_grid(settings):
    '''
    returns a list of all grid positions
    for settings rows and cols
    '''
    rows = range(settings['rows'])
    cols = range(settings['cols'])
    grid = list(itertools.product(cols, rows))
    return grid


def neighbors(pos, settings):
    '''
    list all neighbors of cell at pos [x,y]
    in matrix defined by settings 'rows' and 'cols'
    '''
    X = settings['cols']
    Y = settings['rows']
    x, y = pos
    neibs = []
    for x2 in range(x-1, x+2):
        for y2 in range(y-1, y+2):
            if -1 < x <= X:
                if -1 < y <= Y:
                    if (x != x2 or y != y2):
                        if (0 <= x2 < X) and (0 <= y2 < Y):
                            neibs.append((x2, y2))
    return neibs


def taken_cells(nodes):
    '''
    return all cells occupied by a node
    '''
    taken = []
    for node in nodes:
        taken.append((node['mx'], node['my']))
    return taken

def load_recs(driver):
    '''
    loads and returns all recommendations
    '''
    try:
        element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.TAG_NAME, "ytd-watch-next-secondary-results-renderer")))

        watch_next = driver.find_element_by_tag_name("ytd-watch-next-secondary-results-renderer")
        recs = watch_next.find_elements_by_tag_name("ytd-compact-video-renderer")

        return recs

    except:
        # TODO: re-try here instead of giving up
        print('refs not loaded')
        driver.quit()


def get_vid_data(driver, vid_url):
    ''' Takes in a selenium driver and video url
    Returns title, views, id and timestamp as dict
    '''

    driver.get(vid_url)

    id = vid_url.split('v=')[1]

    main_info = driver.find_element_by_tag_name("ytd-video-primary-info-renderer")
    title_tag = main_info.find_element_by_tag_name("h1")
    title_tag = title_tag.find_element_by_tag_name("yt-formatted-string")
    title = title_tag.get_attribute('innerHTML')

    view_tag = driver.find_element_by_tag_name("yt-view-count-renderer")
    view_counter = view_tag.find_element_by_class_name("view-count")
    views_html = view_counter.get_attribute('innerHTML')
    v = re.findall(r'\d+', views_html)
    views_s = ''.join(v)
    views = int(views_s)

    # uploader name
    meta = driver.find_element_by_tag_name("ytd-video-owner-renderer")
    owner_info = meta.find_element_by_id("channel-name")
    owner_name = owner_info.find_element_by_tag_name('a').get_attribute('innerHTML')
    # date
    date_string = meta.find_element_by_class_name("date").get_attribute('innerHTML')

    r = re.compile('\d+\s[\w\.]+\s\d+')
    try:
        date = r.search(date_string).group(0)
    except:
        date = ''

    node_props = {
        'title': html.unescape(title),
        'views': views,
        'by': owner_name,
        'date': date,
        'YT_id': id}

    return node_props
