import time
import csv
import json
import os

# %%

# %% Settings
settings = {
    'buffer_size': 7,
    'node_size': 240,
    'cols': 6,
    'rows': 3,
    'gap': 50,
    'padding': 30
    }

# %%

replays = 'replays/'

# %%

while True:

    for filename in os.listdir(replays):

        file = open(replays + filename)
        total_rows = len(file.readlines())

        reader = csv.DictReader(open(replays + filename))

        nodes = []
        links = []

        time_stamp = filename.split('.')[0]
        meta = {'start_time': time_stamp, 'steps': 0}

        for line in reader:

            for node in nodes:
                node['age'] += 1

            node = {
                'id': line['id'],
                'text': line['text'],
                'properties': eval(line['properties']),
                'mx': int(line['mx']),
                'my': int(line['my']),
                'x': int(line['x']),
                'y': int(line['y']),
                'age': 0
            }

            nodes.append(node)

            if len(nodes) > settings['buffer_size']:
                nodes.pop(0)

            links = [] # reset
            for v, w in zip(nodes[:-1], nodes[1:]):
                link = {'source': nodes.index(v), 'target': nodes.index(w)}
                links.append(link)

            meta['steps'] = str(node['id']) + '/' + str(total_rows)


            data = {"meta": meta,
                    "settings":settings,
                    "nodes": nodes,
                    "links": links
                    }

            with open('live_trace.json', 'w') as outfile:
                json.dump(data, outfile)

            time.sleep(2)
