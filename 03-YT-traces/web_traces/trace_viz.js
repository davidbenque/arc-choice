d3.json("live_trace.json", function(json) {

var settings = json.settings;

// read this from the JSON
var node_size = settings.node_size;
var cols = settings.cols;
var rows = settings.rows;
var gap = settings.gap;
var padding = settings.padding;
var buffer = settings.buffer_size;

// Create SVG
var height = (function(){
  return rows * (node_size + gap) + padding;
})();

var width = (function(){
  return cols * (node_size + gap) + padding;
})();

var svg = d3.select("#container").append("svg")
    .attr("width", width)
    .attr("height", height);

// arrow marker def
svg.append("svg:defs").selectAll("marker")
    .data(["end"])      // Different link/path types can be defined here
  .enter().append("svg:marker")    // This section adds in the arrows
    .attr("id", String)
    .attr("viewBox", "0 -20 40 40")
    .attr("refX", 10)
    .attr("refY", 0)
    .attr("markerWidth", 40)
    .attr("markerHeight", 40)
    .attr("orient", "auto")
  .append("svg:path")
    .attr("d", "M0,-5 L10,0L0,5");

// title info
var title = d3.select("#title").append("p")
    .html('Trace: Simple Digger | ' +
          'Started: '+ json.meta.start_time + ' | ' +
          'Step: ' + json.meta.steps);



  var num_nodes = Object.keys(json.nodes).length;

// assign positions

json.nodes.forEach(function(e){
  e.x += settings.padding;
  e.y += settings.padding;
 });

json.links.forEach(function(link){
  var sourceNode = json.nodes.filter(function(d, i) {
    return i == link.source
  })[0];
  var targetNode = json.nodes.filter(function(d, i) {
    return i == link.target
  })[0];

  link.age = sourceNode.age;

  // Arrow alignments
  link.x1 = (function() {
    if (sourceNode.x < targetNode.x){
      return sourceNode.x + node_size;
    } else if ((sourceNode.y != targetNode.y) && (sourceNode.x == targetNode.x)){
      return sourceNode.x + node_size/2;
    } else { return sourceNode.x}
  })();
  link.y1 = (function() {
    if(sourceNode.y + node_size < targetNode.y){
      return sourceNode.y + node_size;
    } if(sourceNode.y  > targetNode.y + node_size){
      return sourceNode.y;
    } else { return sourceNode.y + node_size/2;}
  })();
  link.x2 = (function() {
    if (targetNode.x + node_size < sourceNode.x){
      return targetNode.x + node_size;
    } else if ((sourceNode.y != targetNode.y) && (sourceNode.x == targetNode.x)){
      return targetNode.x + node_size/2;
    } else { return targetNode.x }
  })();
  link.y2 = (function() {
    if(targetNode.y > sourceNode.y  + node_size ){
      return targetNode.y ;
    } if(targetNode.y + node_size < sourceNode.y){
      return targetNode.y + node_size ;
    } else { return targetNode.y + node_size/2;}
  })();

});

// create elements

  var links = svg.selectAll(".link")
      .data(json.links)
    .enter().append("line")
      .attr("class", "link")
      .attr("x1", function(d){return d.x1})
      .attr("y1", function(d){return d.y1})
      .attr("x2", function(d){return d.x2})
      .attr("y2", function(d){return d.y2})
      .attr("marker-end", "url(#end)")
      .attr("style", function(d){
        return "opacity:" + (1. - d.age / buffer);
      });

  var node = svg.selectAll(".node")
      .data(json.nodes)
    .enter().append("g")
      .attr("class", "node")
      .attr("style", function(d) {  // AGE OPACITY
        return "opacity:" + (1. - d.age / buffer);
      })
            .attr("transform", function(d) {
              return 'translate (' + d.x + ',' + d.y + ')'
            });


 node.append("rect")
      .attr("width",node_size)
      .attr("height", node_size)
      .attr("class", function(d){
        if (d.text == "Youtube homepage"){ return "yt-home"} else {return "video"}
      });

  node.append("text")
      .text(function(d) { return d.text })
      .attr("class", function(d){
        if (d.text == "Youtube homepage"){ return "YT-home"} else { return "video-title" }
      });


var wrap_title = d3.textwrap()
  .bounds({height: 2*(node_size/3), width: node_size})
  .padding(10);

d3.selectAll(".video-title").call(wrap_title)

d3.selectAll("foreignObject").select("div")
.attr("class", "video-title");


var meta_node = svg.selectAll(".meta-node")
    .data(json.nodes)
  .enter().append("g")
    .attr("class", "meta-node")
    .attr("style", function(d) {  // AGE OPACITY
      return "opacity:" + (1. - d.age / buffer);
    })
    .attr("transform", function(d){return "translate(" + d.x + "," + d.y + ")" });


// meta info : Byline
 meta_node.append("text")
 .attr("class", "author")
     .text(function(d) {
       var byline = d.properties.by;
       if(typeof byline == "undefined"){return '📺' } else { return byline }
     });

var wrap_author = d3.textwrap()
  .bounds({height: 1*(node_size/4), width: node_size})
  .padding(10);

d3.selectAll(".meta-node").selectAll(".author").call(wrap_author);

d3.selectAll(".meta-node").selectAll("foreignObject")
  .attr("y", 2*(node_size/3))
  .selectAll("div")
  .attr("class", "author")


// meta info: date and views
var meta =   meta_node.append("text")
.attr("class", "meta-info")
.attr("transform", "translate(10," + (node_size - 26) + ")"); // position of meta info

  meta.append("tspan")
    .attr("x", 0)
    .text(function(d){
      return d.properties.date;
    });

  meta.append("tspan")
      .attr("x", 0)
      .attr("dy", 18)
    .text(function(d){
      var views = d.properties.views;
      if (typeof views !== 'undefined'){
      return "👁 " + numberWithCommas(d.properties.views)}
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

});
