These scripts generate a 'live' D3.js visualisation in the browser. The .json file is updated every few seconds, with a live local server to watch for changes and automatically refresh the page. I used:
- the live-server package in [Atom](https://atom.io/packages/atom-live-server)
- the same (?) thing from [NPM](https://www.npmjs.com/package/live-server)
- [Beaker Browser](https://beakerbrowser.com/) if you make this directory into a Dat archive and turn on live reloading. This also allows to 'stream' the visualisation across the dat:// protocol (viewers also have to turn on live reloading).


To run the live version:   
- start a live server in this directory
- run `arc_tracer.py`

To replay sessions:   
- place the .csv files you want to play in the `replays` folder
- start a live server in this directory
- run `arc_player.py`
- the 'playlist' was set to repeat forever for the exhibition

example output: 

![](03-YT-traces/web_traces/screencast_trace.mp4)